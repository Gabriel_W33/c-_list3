
#include <gtest/gtest.h>
#include "funkcjakwadratowa.h"
#include "ExceptionQ.h"

TEST(funkcjaKwadratowaTest, Zero)
{
    funkcjaKwadratowa zfun(2,-4, 2);
    EXPECT_EQ(1, zfun.oblicz(1));
    EXPECT_THROW(zfun.oblicz(2),ExceptionQ);

}

TEST(funkcjaKwadratowaTest, One)
{
    funkcjaKwadratowa zfun(-1,3, 4);
    EXPECT_EQ(-1, zfun.oblicz(1));
    EXPECT_EQ(4, zfun.oblicz(2));
}

TEST(funkcjaKwadratowaTest, Two)
{
    funkcjaKwadratowa zfun(-5, 6, -2);
    EXPECT_THROW(zfun.oblicz(2),ExceptionQ);
    EXPECT_THROW(zfun.oblicz(1),ExceptionQ);
}